---
# SPDX-License-Identifier: CC-BY-SA-4.0
# Copyright © 2020 Casper Meijn <casper@meijn.net>
# 
# This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. 
# To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or 
#   send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

layout: recipe
date: 2021-05-24T12:09:55+02:00
title: "Broccoliquiche"
authorName: Casper Meijn
authorURL: https://www.caspermeijn.nl
sourceName: # Naam van de bron van het recept
sourceURL: # Website van de bron van het recept
category: Avondeten
yield: 2 personen
prepTime: 20 minuten
cookTime: 40 minuten

ingredients:
- Tante Fanny quichedeeg
- 300 gram broccoliroosjes
- 200 gram spekjes
- 100 gram parmezaanse kaas
- 200 ml kookroom
- 5 eieren
- 3 bosui

directions:
- Verwarm de oven voor op 200℃
- Kook de broccoli voor 3 minuten, spoel het af met koud water en laat het uitlekken
- Bekleed de quichevorm met het deeg
- Prik gaatjes in het deeg
- Bak de spekjes in een droge koekenpan
- Snij de bosui en rasp de kaas
- Klop het ei los met de kookroom, zout en peper
- Beleg de bodem met de broccoli, bosui, spekjes, kaas
- Giet het ei-mengsel erover
- Bak de quiche 40 minuten in de oven
---
