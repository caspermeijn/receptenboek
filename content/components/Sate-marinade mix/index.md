---
# SPDX-License-Identifier: CC-BY-SA-4.0
# Copyright © 2021 Casper Meijn <casper@meijn.net>
# 
# This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. 
# To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or 
#   send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

layout: recipe
title: "Saté-marinade mix"
authorName: Casper Meijn
authorURL: https://www.caspermeijn.nl
sourceName: Spice Wise, Michel Hanssen
sourceURL: https://www.bol.com/nl/nl/p/spice-wise/9200000036494863/
yield: 1 potje

ingredients:
- 1½ theelepel paprikapoeder
- ½ theelepel knoflook
- ¼ theelepel korianderzaad
- 1½ theelepel bruine basterdsuiker
- ½ theelepel cayennepeper
- 1 theelepel Engelse kerriepoeder
- ¼ theelepel komijnzaad
- ½ theelepel chilivlokken
- ½ theelepel gemberpoeder

directions:
- Mix alle kruiden met elkaar
---
