---
# SPDX-License-Identifier: CC-BY-SA-4.0
# Copyright © 2021 Casper Meijn <casper@meijn.net>
# 
# This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. 
# To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or 
#   send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

layout: recipe
title: "Nasi kruiden"
authorName: Casper Meijn
authorURL: https://www.caspermeijn.nl
sourceName: Spice Wise, Michel Hanssen
sourceURL: https://www.bol.com/nl/nl/p/spice-wise/9200000036494863/
yield: 1 potje

ingredients:
- 1½ theelepel koriander
- 1 theelepel komijnzaad
- ½ theelepel gemberpoeder
- ½ theelepel witte peper
- 1 theelepel knoflook
- ¼ theelepel cayennepeper
- 1 theelepel uienpoeder
- 2 theelepel paprikapoeder

directions:
- Mix alle kruiden met elkaar
---
