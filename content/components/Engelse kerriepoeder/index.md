---
# SPDX-License-Identifier: CC-BY-SA-4.0
# Copyright © 2021 Casper Meijn <casper@meijn.net>
# 
# This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. 
# To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or 
#   send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

layout: recipe
title: "Engelse kerriepoeder"
authorName: Casper Meijn
authorURL: https://www.caspermeijn.nl
sourceName: Spice Wise, Michel Hanssen
sourceURL: https://www.bol.com/nl/nl/p/spice-wise/9200000036494863/
yield: 1 potje

ingredients:
- ¾ theelepel komijnzaad
- 2½ theelepel kurkuma
- 1¼ theelepel korianderzaad
- ½ theelepel fenegriek
- ¼ theelepel mosterdzaad
- ¼ theelepel kardemom
- ¼ theelepel cayennepeper
- ¾ theelepel witte peper
- ½ theelepel gemberpoeder
- ½ theelepel piment
- ¼ theelepel foelie
- ¼ theelepel karwijzaad

directions:
- Mix alle kruiden met elkaar
---
